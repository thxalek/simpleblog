const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { APP_SECRET, getUserId } = require('../utils');

async function signup(root, args, context, info) {

  const password = await bcrypt.hash(args.password, 10)

  const user = await context.db.mutation.createUser({
    data: { ...args,
      password
    },
  }, `{ id }`)

  const token = jwt.sign({
    userId: user.id
  }, APP_SECRET)

  return {
    token,
    user,
  }
}

async function login(root, args, context, info) {

  const user = await context.db.query.user({
    where: {
      email: args.email
    }
  }, ` { id password } `)
  if (!user) {
    throw new Error('No such user found')
  }

  const valid = await bcrypt.compare(args.password, user.password)
  if (!valid) {
    throw new Error('Invalid password')
  }

  const token = jwt.sign({
    userId: user.id
  }, APP_SECRET)

  return {
    token,
    user,
  }
}

async function createPost(root, args, context, info) {
  const userId = getUserId(context)

  if (!userId) {
    throw new Error('No such user found');
  }

  return context.db.mutation.createPost({
      data: {
        title: args.title,
        description: args.description,
        content: args.content,
        postedBy: {
          connect: {
            id: userId
          }
        },
      },
    },
    info,
  )
}

async function deletePost(root, args, context, info) {
  const userId = getUserId(context);

  if (!userId) {
    throw new Error('No such user found');
  }

  const post = await context.db.query.post({
    where: {
      id: args.id
    }
  }, ` { postedBy { id } } `);

  if (!post) {
    throw new Error('No such post found');
  }

  if (userId !== post.postedBy.id) {
    throw new Error('Access to delete this post is denied.');
  }


  return context.db.mutation.deletePost({
      where: {
        id: args.id
      }
    },
    info,
  )
}

async function updatePost(root, args, context, info) {
  const userId = getUserId(context);

  if (!userId) {
    throw new Error('No such user found');
  }

  const post = await context.db.query.post({
    where: {
      id: args.id
    }
  }, ` { postedBy { id } } `);

  if (!post) {
    throw new Error('No such post found');
  }

  if (userId !== post.postedBy.id) {
    throw new Error('Access to update this post is denied.');
  }

  return context.db.mutation.updatePost({
      where: {id: args.id },
      data: {
        title: args.title,
        description: args.description,
        content: args.content
      },
    },
    info,
  )
}

async function createComment(root, args, context, info) {
  const userId = getUserId(context);

  if (!userId) {
    throw new Error('No such user found');
  }

  const post = await context.db.query.post({
      where: {
        id: args.parent
      }
    },
    `{ id }`
  );

  if (!post) {
    throw new Error('No such post found');
  }

  return context.db.mutation.createComment({
      data: {
        content: args.content,
        parent: {
          connect: {
            id: args.parent
          }
        },
        postedBy: {
          connect: {
            id: userId
          }
        },
      },
    },
    info,
  )
}

async function deleteComment(root, args, context, info) {
  const userId = getUserId(context);

  if (!userId) {
    throw new Error('No such user found');
  }

  const comment = await context.db.query.comment({
    where: {
      id: args.id
    }
  }, ` { postedBy { id } } `);

  if(!comment) {
    throw new Error('No such comment found');
  }

  if (userId !== comment.postedBy.id) {
    throw new Error('Access to delete this comment is denied.');
  }

  return context.db.mutation.deleteComment({
      where: {
        id: args.id
      },
    },
    info,
  )
}

async function updateComment(root, args, context, info) {
  const userId = getUserId(context);

  if (!userId) {
    throw new Error('No such user found');
  }

  const comment = await context.db.query.comment({
    where: {
      id: args.id
    }    
  }, ` { postedBy { id } } `);

  if(!comment) {
    throw new Error('No such comment found');
  }

  if (userId !== comment.postedBy.id) {
    throw new Error('Access to update this comment is denied.');
  }

  return context.db.mutation.updateComment({
      where: {
        id: args.id
      },
      data: {
        content: args.content
      },
    },
    info,
  )
}

async function createVote(root, args, context, info) {
  const userId = getUserId(context);

  if (!userId) {
    throw new Error('No such user found');
  }

  const voteExists = await context.db.exists.Vote({
    user: { id: userId },
    post: { id: args.post },
  })

  if (voteExists) {
    throw new Error(`Already voted for link: ${args.post}`)
  }

  const post = await context.db.query.post({
      where: {
        id: args.post
      }
    },
    `{ id }`
  );

  if (!post) {
    throw new Error('No such post found');
  }

  return context.db.mutation.createVote({
      data: {
        post: {
          connect: {
            id: args.post
          }
        },
        user: {
          connect: {
            id: userId
          }
        },
      },
    },
    info,
  )
}

async function deleteVote(root, args, context, info) {
  const userId = getUserId(context);

  if (!userId) {
    throw new Error('No such user found');
  }

  return context.db.mutation.deleteVote({
      where: {
        id: args.id
      },
    },
    info
  )
}

module.exports = {
  signup,
  login,
  createPost,
  deletePost,
  updatePost,
  createComment,
  deleteComment,
  updateComment,
  createVote,
  deleteVote
}