function post(root, args, context, info) {
  return context.db.query.post({
    where: {
      id: args.id
    }
  }, info);
}

function posts(root, args, context, info) {
  return context.db.query.posts({}, info);
}

function comment(root, args, context, info) {
  return context.db.query.comment({
    where: {
      id: args.id
    }
  }, info);
}

function comments(root, args, context, info) {
  return context.db.query.comments({
    where: {
      parent: args.parent
    }
  }, info);
}

module.exports = {
  post,
  posts,
  comment,
  comments
}