import gql from 'graphql-tag'

export const ALL_POSTS_QUERY = gql`
  query AllPostsQuery {
    posts {
      id
      title
      createdAt
      description
      postedBy {
        id
        name
      }
    }
  }
`

export const CREATE_POST_MUTATION = gql`
  mutation CreatePostMutation($title: String!, $description: String!, $content: String!) {
    createPost(
      title: $title,
      description: $description,
      content: $content
    ) {
      id
      createdAt
      title
      description
      postedBy {
        id
        name
      }
    }
  }
`

export const CREATE_USER_MUTATION = gql`
  mutation SignupMutation($name: String!, $email: String!, $password: String!) {
    signup(
      email: $email,
      password: $password,
      name: $name
    ) {
      token
    }

    login(
      email: $email,
      password: $password
    ) {
      token
      user {
        id
      }
    }
  }
`

export const SIGNIN_USER_MUTATION = gql`
  mutation SigninMutation($email: String!, $password: String!) {
    login(
      email: $email,
      password: $password
    ) {
      token
      user {
        id
      }
    }
  }
`
