import Vue from 'vue'
import Router from 'vue-router'
import PostList from '@/components/PostList'
import CreatePost from '@/components/CreatePost'
import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    component: PostList
  },
  {
    path: '/create',
    component: CreatePost
  },
  {
    path: '/login',
    component: Login
  }],
  mode: 'history'
})
